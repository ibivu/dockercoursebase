#!/bin/bash

set -e

service shellinabox start

exec /usr/sbin/sshd -D
