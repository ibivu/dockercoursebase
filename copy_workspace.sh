#!/bin/bash
DIR="/workspace/"

if [ "$(ls -A $DIR)" ]; then
     echo "Nothing to be done: $DIR is not empty"
else
    echo "$DIR is empty, initializing with files from template directory"
    cp -R /workspace_template/* /workspace/ && chown -R student:student /workspace
fi
