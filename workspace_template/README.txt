This is the workspace template for the IBIVU course base docker image. In any
subimage inheriting from this image you should put an initial state for the
workspace here, including a replacement README.txt to guide the student along.
